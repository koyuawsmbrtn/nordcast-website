document.addEventListener("DOMContentLoaded", function(event) { 
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
        document.getElementById("logo").setAttribute("src", e.matches ? "https://web.nordcast.app/logo_dark.png" : "https://web.nordcast.app/logo.png");
    });
    
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        document.getElementById("logo").setAttribute("src", "https://web.nordcast.app/logo_dark.png");
    }
});